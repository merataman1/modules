#!/usr/bin/env nextflow

nextflow.enable.dsl=2

process PROTEIN_PROPHET {
	stageInMode "copy"

	input:
	path meta
	path db
	path(pep)
	val params

	output:
	path('interact.prot.xml'), emit: interact_prot
	path '.meta', emit: meta

	shell:
	'''
	ppp --path "$PWD" "!{meta}/meta.bin"
	philosopher proteinprophet !{params} !{pep}
	'''
}
