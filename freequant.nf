#!/usr/bin/env nextflow

nextflow.enable.dsl=2

process FREEQUANT {
	stageInMode "copy"

	input:
	tuple path(meta), path(group_dir)
	path pep
	path prots

	output:
	tuple path('.meta'), path(group_dir)

	shell:
	'''
	ppp --path "$PWD" "!{meta}/meta.bin"
	philosopher freequant --dir ./$(basename !{group_dir})
	'''
}
